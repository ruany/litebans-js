@file:JsExport
import kotlin.math.*
import kotlinx.browser.document

@JsExport
fun getSeedValue(str: String): Long = str.hashCode().toLong()

class XorShift(seed: Number) {
    val s = longArrayOf(seed.toLong(), seed.toLong())

    fun nextLong(): Long {
        var x = s[0]
        val y = s[1]
        s[0] = y
        x = x.xor(x.shl(23))
        x = x.xor(y).xor((x.shr(17)).xor(y.shr(26)))
        s[1] = x
        return (x + y)
    }

    open fun nextInt(bound: Int): Int {
        require(bound > 0) { bound.toString() }
        val next = abs(nextLong().toInt().rem(bound))
        require(next >= 0)
        return next
    }
}

class ObscureID(seed: Number) {
    val shuffleSecret: Int

    val shuffle = hashMapOf<Int, String>()
    val unshuffle = hashMapOf<String, Int>()

    init {
        val rng = XorShift(seed)
        shuffleSecret = 8500 + rng.nextInt(4000)
        buildShuffleTables(rng)
    }

    companion object {
        const val BLOCK_SIZE = 3
        const val MINIMUM_OUTPUT_SIZE = (BLOCK_SIZE*2)
        const val ERROR = -1L
    }

    class ObscureParsingFailure(message: String = "") : RuntimeException(message)

    fun obscure(input: Number): String {
        try {
            val secret = (input.toLong() + shuffleSecret)
            var id = secret.toString()
            id = leftPadInflate(id)

            val output = StringBuilder()
            val currentBlock = StringBuilder()
            var firstBlock = true
            for (char in id) {
                currentBlock.append(char)
                if (currentBlock.length >= BLOCK_SIZE) {
                    if (!firstBlock) {
                        if (currentBlock.startsWith("000")) output.append("m")
                        else if (currentBlock.startsWith("00")) output.append("v")
                        else if (currentBlock.startsWith("0")) output.append("z")
                    }
                    output.append(shuffleIn(currentBlock))
                    currentBlock.setLength(0)
                    firstBlock = false
                }
            }
            if (currentBlock.isNotEmpty()) throw ObscureParsingFailure()
            return output.toString().uppercase()
        } catch (ex: ObscureParsingFailure) {
            return "error"
        }
    }

    private fun leftPadInflate(id: String) = when (id.length.rem(BLOCK_SIZE)) {
        1 -> "00$id"
        2 -> "0$id"
        else -> id
    }

    private fun leftPadDeflate(pad: Int, result: Int) = when (pad) {
        0 -> result.toString()
        1 -> "0$result"
        2 -> "00$result"
        3 -> "000"
        else -> throw ObscureParsingFailure()
    }

    fun reveal(input: String): Number {
        try {
            val str = input.lowercase()
            val output = StringBuilder()
            val currentBlock = StringBuilder()
            var pad = 0
            for (char in str) {
                when (char) {
                    'm' -> pad = 3
                    'v' -> pad = 2
                    'z' -> pad = 1
                    else -> currentBlock.append(char)
                }
                if (currentBlock.length >= BLOCK_SIZE) {
                    output.append(shuffleOut(currentBlock, pad))
                    currentBlock.setLength(0)
                    pad = 0
                }
            }
            if (currentBlock.isNotEmpty()) throw ObscureParsingFailure()
            return (output.toString().toLong() - shuffleSecret)
        } catch (ex: ObscureParsingFailure) {
            return ERROR
        }
    }

    private fun buildShuffleTables(rng: XorShift) {
        for (key in (0..999)) {
            var hex = nextHexSequence(rng)
            while (hex.length != BLOCK_SIZE || hex in unshuffle) hex = nextHexSequence(rng)

            shuffle[key] = hex
            unshuffle[hex] = key
        }
    }

    private fun nextHexSequence(rng: XorShift) = (rng.nextLong().and(0xfff)).toString(16)

    private fun shuffleIn(currentBlock: StringBuilder) = shuffle[currentBlock.toString().toInt()] ?: throw ObscureParsingFailure("No shuffle input for $currentBlock")
    private fun shuffleOut(currentBlock: StringBuilder, pad: Int): String {
        val result = unshuffle[currentBlock.toString()] ?: throw ObscureParsingFailure("No shuffle output for $currentBlock")
        return leftPadDeflate(pad, result)
    }
}