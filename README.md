## Usage

```
> var litebans = require('litebans-js-client');
> var seed = litebans.getSeedValue("1234")
> var ob = new litebans.ObscureID(seed)
> ob.obscure(345789)
'E328B4'
> ob.reveal('E328B4').toString()
'345789'
```